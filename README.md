# TreeFramework

#### 介绍
用Unity搭建一个自己的游戏框架，适合想学习从0开始一步一步搭建自己的游戏框架的同学


#### 软件架构
框架已经集成HybridCLR C#热更新方案和YooAsset资源管理系统


#### 安装教程

B站教程：https://www.bilibili.com/video/BV1jM411P7K3/?spm_id_from=333.788&vd_source=c451db3adf697844895358c67f55e80f

#### 使用说明

参照B站教程

#### 开发计划

1.  集成luban数据配置表工具
2.  集成FairyGUI
3.  UI框架
